# Bootybot

This is a script for doing things with Plex that make it easier to do other
things.  I'm not going to say what things those are, but we all know.

## Dependencies

* Filebot

* `unrar`

* Python 3

## Configuration

The default config location is `~/.config/bootybot.conf`.  Below is an example.

```json
{
	"outdir": "/mnt/xvol1/plexdata",
	"extractdir": "/tmp/bootybot_extract",
	"simpleaction": "copy",
	"overrides": [
		{
			"name": "The Simpsons",
			"pattern": "The\\.Simpsons\\..*"
		}
	]
}
```

The `outdir` property has the `{plex}` formatter from Filebot appended to it,
so you should not have to worry about handling different media classes (TV,
Movies, etc.).

The `extractdir` is where we extract data from RAR archives into.  It usually
ends up being emptied after we finish processing, as we move data out of here
after extraction.

The `simpleaction` property is passed to Filebot when processing simple,
non-archived media files.  I use `copy` because of how I want to handle dealing
with data *after* it's been loaded into Plex, but you might want to use `move`,
`hardlink`, or `keeplink` depending on what you're situation is like.

The `overrides` section is used to enforce that TV shows have their names
properly auto-detected, as occasionally Filebot trips up and misses it.  If none
of the entries match the file then we just hope that Filebot figures it out on
its own.  Note that the regexes must match the *entire* filename.  So it's a
good idea to put a `.*` at the end to make sure it matches every file format
and from any "distributor".

## Usage

Once you've configured it, you can just run `booty.py` in the directory of the
"media" you're trying to prepare.

You can set the `BOOTYCFG` envvar to override the config location.
