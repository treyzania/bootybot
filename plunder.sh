#!/bin/bash

MAIN_CONFIG="~/.config/bootybot/plunder.conf"
if [ -f localconfig ]; then
	source localconfig-plunder
elif [ -f "$MAIN_CONFIG" ]; then
	source $MAIN_CONFIG
else
	echo 'error: no valid config found'
	exit 1
fi

# This is from the Deluge Execute plugin.
tid=$1
tname=$2
tpath=$3

########

troot="$tpath/$tid"
rxpath="$remote_rx_dir/$tid"
mvpath="$remote_mv_dir/$tid"

function remote_exec () {
	ssh $ssh_opts $ssh_prop $@
}

# Setup.
remote_exec mkdir -p $remote_rx_dir
remote_exec mkdir -p $remote_mv_dir

# Actual transfer.
remote_exec mkdir "$txpath"
scp -r $troot $ssh_prop:"$remote_rx_dir"

# Cleanup.  This should be very fast.
remote_exec mv "$rxpath" "$mvpath"

# TODO Automaticially invoke (& fork) the rename script?

